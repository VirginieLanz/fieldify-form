import React from 'react'
import { Schema, Types, Input } from '@fieldify/antd'
import { Button, Alert } from 'antd';
import "antd/dist/antd.css";
import "./index.css";
import axios from 'axios';

const {
  FieldifySchemaForm
} = Schema

// The class RandomNumber is just here for me to exercise a bit
// The ideal would be to send a get request to a db to opt the right information
class RandomNumber extends React.Component {
  constructor(props){
    super(props);
    this.handleLoad = this.handleLoad.bind(this);
    // Initialize the state to random 0 
    this.state = { random: 0 };
  }

  // Method called when all the elements on the page is loaded correctly
  componentDidMount() {
    window.addEventListener('load', this.handleLoad);
 }

 componentWillUnmount() { 
   window.removeEventListener('load', this.handleLoad)  
 }

  handleLoad(){
    // Random number generated between 1000 and 10'000
    const min = 1000;
    const max = 10000;
    const rand = min + Math.random() * (max - min);
    // Cast float to int
    const int = Math.round(rand)
    this.setState({random: this.state.random + int});
  }

  render(){
    return(
      <div>
        {this.state.random}
      </div>
    );
  }
}


export default class App extends React.Component {
  constructor(props) {
    super(props)

    const nativ = {
      name: {
        $doc: "Nom et prénom",
        $type: "Name",
      },

      email: {
        $doc: "E-mail",
        $type: "Email",
        $options: {placeholder: "votremail@exemple.ch"},
      }, 

      address: {
        $doc: "Adresses",
        home: {
          $doc: "Facturation",
          street: {
            $doc: "Nom de rue", $type: "String", $options: {min: 2, placeholder: "Rue des Champs" } },
          streetNumber: { 
            $doc: "Numéro de rue", $type: "Number", $options: {placeholder: "13" } },
          zip: { 
            $doc: "Code postal", $type: "Number", $options: { placeholder: "1400"} },
          country: { 
            $doc: "Pays", $type: "Select", 
            $options: {
              default: "Suisse",
              items: {
                suisse: "Suisse",
                france: "France",
                belgique: "Belgique"
              }
            } 
          },
        },
        work: {
          $doc: "Livraison",
          street: {
            $doc: "Nom de rue", $type: "String", $options: {min: 2, placeholder: "Rue des Champs" } },
          streetNumber: { 
            $doc: "Numéro de rue", $type: "Number", $options: {placeholder: "13" } },
          zip: { 
            $doc: "Code postal", $type: "Number", $options: { placeholder: "1400"} },
          country: { 
            $doc: "Pays", $type: "Select", 
            $options: {
              default: "Suisse",
              items: {
                suisse: "Suisse",
                france: "France",
                belgique: "Belgique"
              }
            } 
          },
        },
      }
    }
    this.state = {
      schema: nativ, 
      input: {},
      error: null
    }
  }

  handleChange(input, value) {
    input.verify((result) => {

      const state = {
        inputRender: { ...result.result },
      }

      if (result.error === true) {
        console.log("Wrong input")
      }else {
        console.log("Right input")
      }
      this.setState(state)
    })
  }

  handleSubmit = (e) => {
    // Prevent to reload the window
    e.preventDefault();
    console.log("submit button clicked")

    try {
      const data = JSON.stringify(this.state.input)
    // Send a POST request to the bd
    axios
      .post('/', {data})
      
      .then(function (response) {
        console.log(response);
        console.log('form informations properly entered');
      })

      .catch(function (error) {
        if (error.response){
          console.log(error)
          // Todo: if 404, redirect to page 404. If 5XX, display another message, etc
        } else if (error.request){
          // Happens when the browser was able to make a request, but for some reason, it didn't see a response.
          // Can be a lot of things, console.log('Network Error') doesn't help
          console.log(error.request)
        } else {
          console.log(error)
          // Not an axios error => do stack trace
        }
      });
    } catch (error) {
      this.setState({error});
      console.log("Erreur:" + this.state.error)
    }

  }

  render() {
    const title = "Commande n°";
    const handleSubmit = this.state.handleSubmit;
    const style = {
      padding: "30px",
      with: "100%"
    };
    if (this.state.error) {
      return <h1>oops</h1>
    }

    return <div>
      <body style={style}>
        {/*{data}*/}
        <h1>{title}<RandomNumber></RandomNumber></h1>
        {/*{JSON.stringify(this.state.input)}*/}
        <FieldifySchemaForm schema={this.state.schema} input={this.state.input} onChange={this.handleChange.bind(this)} />
        <Button onClick={this.handleSubmit} type="primary">Envoyer</Button>
      </body>
    </div>
  }
}



